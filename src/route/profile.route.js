const express = require("express");
const {
  getProfileUser,
  updateProfile,
} = require("../controllers/profile.controllers");
const { authenticateToken } = require("../middleware/auth.token");

const route = express.Router();

route.get("/", authenticateToken, getProfileUser);

route.patch("/", authenticateToken, updateProfile);

module.exports = route;
