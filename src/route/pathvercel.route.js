const express = require("express");
const route = express.Router();

route.get("/", (req, res) => {
  return res.json({
    info: {
      creator: "Ichal Wira",
      gitlab: "https://gitlab.com/IchAlwiros/backend-tecnoverse",
      github: "https://github.com/IchalAlwiros",
    },
  });
});

module.exports = route;
