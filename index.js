const express = require("express");
const cors = require("cors");
const dotenv = require("dotenv");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./src/doc/documentation.json");

dotenv.config();
const app = express();
const corsOptions = {
  origin: "*",
};

//MIDDLEWARE
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

// ROUTE
require("./src/route")(app);

//  DOCUMENTATION REST API WITH SWAGGER UI
// custom css url options
const options = {
  customCssUrl: "https://unpkg.com/swagger-ui-dist@4.5.0/swagger-ui.css",
};

app.use(
  "/api-docs",
  swaggerUi.serve,
  swaggerUi.setup(swaggerDocument, options)
);

// PORT
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Listen Port ${PORT}`);
});
