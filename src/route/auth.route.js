const express = require("express");

const {
  login,
  register,
  changePassword,
  findUser,
} = require("../controllers/user.controllers");

const { authenticateToken } = require("../middleware/auth.token");

const route = express.Router();

route.post("/register", register);

route.post("/login", login);

route.post("/change-password", authenticateToken, changePassword);

route.get("/", authenticateToken, findUser);

module.exports = route;
