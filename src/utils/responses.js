class ResponseApi {
  static responseSucces = (res, statusCode, data, message) => {
    return res.status(statusCode).json({
      statusCode,
      message,
      data: data,
    });
  };

  static responseError = (res, statusCode, error, message) => {
    return res.status(statusCode).json({
      statusCode,
      message,
      error: error,
    });
  };
}

module.exports = ResponseApi;
