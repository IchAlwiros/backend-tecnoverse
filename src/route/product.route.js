const express = require("express");
const {
  getProduct,
  createProduct,
  updateProduct,
  deleteProduct,
  getProductId,
  getScanProduct,
} = require("../controllers/product.controllers");
const { authenticateToken } = require("../middleware/auth.token");

const route = express.Router();

route.get("/", authenticateToken, getProduct);

route.get("/:id", authenticateToken, getProductId);

route.post("/scan", authenticateToken, getScanProduct);

route.post("/", authenticateToken, createProduct);

route.patch("/:id", authenticateToken, updateProduct);

route.delete("/:id", authenticateToken, deleteProduct);

module.exports = route;
