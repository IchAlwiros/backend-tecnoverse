module.exports = (app) => {
  const User = require("./auth.route");
  app.use("/user", User);

  const Profile = require("./profile.route");
  app.use("/profile", Profile);

  const Product = require("./product.route");
  app.use("/product", Product);

  const Vercel = require("./pathvercel.route");
  app.use("/", Vercel);
};
