const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

let { TOKEN_SECRET } = process.env;

const { responseError, responseSucces } = require("../utils/responses");

class User {
  static register = async (req, res) => {
    let { username, email, password } = req.body;
    const hashedPassword = await bcrypt.hash(password, 10);

    email = email.toLowerCase();

    let validation = [];

    // Email Validation
    if (!email) {
      let err = { email: "email field is required" };
      validation = [...validation, err];
    }
    if (!email.includes("@")) {
      let err = { email: `email must contain the @ character` };
      validation = [...validation, err];
    } else if (!email.endsWith(".com")) {
      let err = { email: `the email must end in .com` };
      validation = [...validation, err];
    }

    // Password Validation
    if (!password) {
      let err = { password: "password field is required" };
      validation = [...validation, err];
    }
    if (password.length <= 6) {
      let err = { password: `password must be longer than 6 characters` };
      validation = [...validation, err];
    } else if (!/\d/.test(password) || !/[a-zA-Z]/.test(password)) {
      let err = { password: `The password must contain words and numbers` };
      validation = [...validation, err];
    }

    // Validation Cek Find Existing Email & Username
    try {
      let existingEmail = await prisma.user.findFirst({
        where: {
          email,
        },
      });
      let existingUsername = await prisma.user.findFirst({
        where: {
          username,
        },
      });

      if (existingEmail) {
        let err = {
          email: `sorry ${email} was exist, please change your email`,
        };
        validation = [...validation, err];
      }

      if (existingUsername) {
        let err = {
          username: `sorry ${username} was exist, please change your username`,
        };
        validation = [...validation, err];
      }
    } catch (error) {
      responseError(res, 500, error, "SERVER ERROR");
    }

    if (validation.length > 0) {
      responseError(res, 400, validation, "BAD REQUEST");
    } else {
      try {
        const user = await prisma.user.create({
          data: {
            username,
            email,
            password: hashedPassword,
          },
        });

        const uuid = user.uuid;

        const expiredIn = 2 * 24 * 3600;

        const token = jwt.sign(
          { userId: user.id, username, email },
          TOKEN_SECRET,
          { expiresIn: expiredIn }
        );
        this.createProfile(user);

        responseSucces(
          res,
          201,
          { uuid, username, email, token },
          "SUCCESS CREATE USER"
        );
      } catch (error) {
        if (error.code === "P2002" && error.meta.target[0] === "email") {
          let err = {
            email: `sorry ${email} was exist, please change your email`,
          };
          validation = [...validation, err];
        }

        if (error.code === "P2002" && error.meta.target[0] === "username") {
          let err = {
            username: `sorry ${username} was exist, please change your email`,
          };
          validation = [...validation, err];
        }

        if (validation.length > 0) {
          responseError(res, 401, validation, "REGISTER FAILED");
        }
      }
    }
  };

  static login = async (req, res) => {
    let { username, email, password } = req.body;

    // email = email.toLowerCase();

    try {
      const user = await prisma.user.findFirstOrThrow({
        where: {
          OR: [{ username }, { email }],
        },
      });

      const valid = bcrypt.compareSync(password, user.password);

      if (!valid) {
        responseError(
          res,
          400,
          " Sorry you can't login your email or password is wrong",
          "FAILED LOGIN"
        );
      } else {
        const expiredIn = 2 * 24 * 3600;

        const token = jwt.sign(
          {
            userId: user.id,
            username: user.username,
            email: user.email,
          },
          TOKEN_SECRET,
          { expiresIn: expiredIn }
        );
        this.createProfile(user);
        responseSucces(
          res,
          200,
          {
            email: user.email,
            username: user.username,
            token: token,
            uuid: user.uuid,
          },
          "LOGIN SUCCESS"
        );
      }
    } catch (error) {
      if (error.code === "P2025") {
        responseError(res, 404, error, "FAILED LOGIN");
      } else {
        responseError(res, 400, error, "FAILED LOGIN");
      }
    }
  };

  static createProfile = async (user) => {
    const getProfile = await prisma.profile.findMany({
      where: {
        id: user.id,
      },
    });

    if (getProfile.length === 0) {
      try {
        const profile = await prisma.profile.create({
          data: {
            userId: user.id,
          },
        });
      } catch (error) {
        console.log("");
      }
    }
  };

  static changePassword = async (req, res) => {
    let { uuid } = req.user;

    const user = await prisma.user.findFirstOrThrow({
      where: {
        OR: [{ uuid }],
      },
    });

    let validation = {};

    if (user) {
      let { oldpassword, newpassword, confirmpassword } = req.body;
      const cekCurrentPassword = bcrypt.compareSync(oldpassword, user.password);
      const cekNewPassword = bcrypt.compareSync(newpassword, confirmpassword);

      if (!cekCurrentPassword) {
        validation = {
          ...validation,
          oldpassword: `sorry your old password is wrong`,
        };
      }

      if (newpassword != confirmpassword) {
        validation = {
          ...validation,
          newpassword: `Sorry, your new password is not the same as the one you entered `,
        };
      }

      if (Object.keys(validation).length > 0) {
        responseError(res, 400, validation, "BAD REQUEST");
      } else {
        let { id, password } = user;
        password = confirmpassword;
        const hashedPassword = await bcrypt.hash(password, 10);
        const updatePassword = await prisma.user.update({
          where: {
            id,
          },
          data: {
            password: hashedPassword,
          },
        });
        responseSucces(
          res,
          201,
          {
            username: updatePassword.username,
            email: updatePassword.email,
            updatedAt: updatePassword.updatedAt,
          },
          "PASSWORD CHANGED"
        );
      }
    }
  };

  static findUser = async (req, res) => {
    let { uuid } = req.user;

    try {
      let data = await prisma.user.findFirstOrThrow({
        where: {
          uuid: uuid,
        },
      });

      responseSucces(res, 200, data, "SUCCES GET DATA");
    } catch (error) {
      responseError(res, 400, error, "BAD REQUEST");
    }
  };
}

module.exports = User;
