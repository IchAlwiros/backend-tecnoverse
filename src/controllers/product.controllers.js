const { PrismaClient } = require("@prisma/client");
const { responseError, responseSucces } = require("../utils/responses");
const prisma = new PrismaClient();

class Product {
  static getProduct = async (req, res) => {
    try {
      let data = await prisma.product.findMany();
      responseSucces(res, 200, data, "SUCCESS GET ALL DATA");
    } catch (error) {
      responseError(res, 400, error, "BAD REQUEST");
    }
  };

  static getProductId = async (req, res) => {
    const { id } = req.params;
    try {
      let data = await prisma.product.findFirstOrThrow({
        where: {
          id: parseInt(id),
        },
      });

      console.log(data);
      responseSucces(res, 200, data, "SUCCESS GET ALL DATA");
    } catch (error) {
      responseError(res, 400, error, "BAD REQUEST");
    }
  };

  static getScanProduct = async (req, res) => {
    const { code } = req.body;
    console.log("data barcode");

    console.log(code);
    try {
      let data = await prisma.product.findFirst({
        where: {
          code,
        },
      });

      console.log(data);
      responseSucces(res, 200, data, "SUCCESS GET ALL DATA");
    } catch (error) {
      responseError(res, 400, error, "BAD REQUEST");
    }
  };

  static createProduct = async (req, res) => {
    let { name, price, description, code } = req.body;
    const { id } = req.user;

    let validation = [];

    if (!name) {
      let err = { name: "name product wajib diisi" };
      validation = [...validation, err];
    }

    if (validation.length > 0) {
      return responseError(res, 400, validation, "BAD REQUEST");
    } else {
      try {
        const data = await prisma.product.create({
          data: {
            name,
            price,
            description,
            code,
            isDeleted: false,
            updatedAt: new Date(),
            user: {
              connect: {
                id: id,
              },
            },
          },
        });
        responseSucces(res, 201, data, "SUCCES CREATE DATA");
      } catch (error) {
        responseError(res, 500, error.messege, "SERVER ERROR");
      }
    }
  };

  static updateProduct = async (req, res) => {
    let { name, price, description, code } = req.body;
    const { id } = req.params;

    let validation = [];

    if (!name) {
      let err = { name: "name product wajib diisi" };
      validation = [...validation, err];
    }

    if (validation.length > 0) {
      return responseError(res, 400, validation, "BAD REQUEST");
    } else {
      try {
        const data = await prisma.product.update({
          where: {
            id: parseInt(id),
          },
          data: {
            name,
            price,
            description,
            code,
            isDeleted: false,
            updatedAt: new Date(),
          },
        });
        responseSucces(res, 201, data, "SUCCES UPDATE DATA");
      } catch (error) {
        responseError(res, 500, error.messege, "SERVER ERROR");
      }
    }
  };

  static deleteProduct = async (req, res) => {
    const { id } = req.params;
    try {
      const existProduct = await prisma.product.findUnique({
        where: {
          id: +id,
        },
      });

      if (!existProduct) {
        return responseError(res, 400, "error", "Product not found");
      }
      //   SOFT DELETE
      const deletedProduct = await prisma.product.update({
        where: {
          id: +id,
        },
        data: {
          isDeleted: true,
          deletedAt: new Date(),
          updatedAt: new Date(),
        },
      });

      return responseSucces(
        res,
        200,
        deletedProduct,
        "Product successfully deleted"
      );
    } catch (error) {
      return responseError(res, 400, "error", "Failed to delete product");
    }
  };
}

module.exports = Product;
