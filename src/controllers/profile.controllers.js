const { PrismaClient } = require("@prisma/client");
const { responseSucces, responseError } = require("../utils/responses");
const prisma = new PrismaClient();

class Profile {
  static getProfileUser = async (req, res) => {
    let { id, uuid } = req.user;
    console.log(uuid, id);
    try {
      let findUser = await prisma.user.findFirstOrThrow({
        where: {
          uuid,
        },
      });
      console.log(findUser.id);
      let profileData = await prisma.profile.findFirst({
        where: {
          userId: findUser.id,
        },
      });

      responseSucces(res, 200, profileData, "SUCCESS GET PROFILE");
    } catch (error) {
      responseError(res, 400, error, "BAD REQUEST");
    }
  };

  static updateProfile = async (req, res) => {
    let { id, uuid } = req.user;
    let { birthdate, address, fullname } = req.body;

    let validation = [];

    if (!address || !fullname) {
      let err = { field: `field address or first_name or last_name required` };
      validation = [...validation, err];
    }

    try {
      let findUser = await prisma.user.findFirst({
        where: {
          uuid,
        },
      });
      // console.log(uuid, findUser);
      let profileUpdate = await prisma.profile.update({
        where: {
          userId: id,
        },
        data: {
          birthdate,
          address,
          fullname,
          updatedAt: new Date(),
        },
      });
      responseSucces(res, 200, profileUpdate, "PROFILE SUCCESS UPDATED");
    } catch (error) {
      if (error.code === "P2025") {
        let err = { userId: `sorry userId was not exist` };
        validation = [...validation, err];
      }

      if (validation.length > 0) {
        responseError(res, 400, validation, "UPDATED PROFILE FAILED");
      }
      responseError(res, 500, error, "SERVER ERROR");
    }
  };
}

module.exports = Profile;
