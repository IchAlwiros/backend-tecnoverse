const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();
const jwt = require("jsonwebtoken");
const { responseSucces, responseError } = require("../utils/responses");

const authenticateToken = (req, res, next) => {
  const authHeader = req.headers["authorization"];

  const token = authHeader && authHeader.split(" ")[1];

  if (token == null) {
    return responseError(res, 401, err, "Unauthorized");
  }

  jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
    // console.log(err);
    if (err) return responseError(res, 401, err, "Invalid Token");
    prisma.user
      .findUnique({
        where: {
          id: user.userId,
        },
      })
      .then((data) => {
        req.user = data;

        next();
      })
      .catch((err) => {
        return responseError(res, 400, err, "Failed to retrieved user data");
      });
  });
};

module.exports = {
  authenticateToken,
};
