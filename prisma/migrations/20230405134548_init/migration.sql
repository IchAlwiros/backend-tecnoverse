/*
  Warnings:

  - Added the required column `code` to the `product` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "product" ADD COLUMN     "code" VARCHAR(192) NOT NULL;
